(** Vector order properties. *)

(* Copyright (C) 2018 Quentin Gougeon

This programm is under GPL license version 3.

See https://www.gnu.org/licenses/gpl.html for details.*)

Require Import List.
Require Export vector_def.
Require Import vector_group.

(* begin hide *)
 Definition le_l (J:nat -> Prop) := expand_predicate2_list J Z.le.
 Definition eq_l (J:nat -> Prop) := expand_predicate2_list J Z.eq.


 Lemma le_l_useful :
  forall (t u:list Z) (J:nat -> Prop), 
   expand_predicate2_list J Z.le t u <-> le_l J t u.
 Proof.
  intros t u ; unfold le_l ; split ; intro H ; assumption.
 Qed.

 Lemma le_trans_list :
  forall (t u v:list Z) (J:nat -> Prop),
   le_l J t u -> le_l J u v -> le_l J t v.
 Proof.
  induction t.
  +induction u ; induction v ; unfold le_l ; simpl ; trivial.
  +intro u ; destruct u.
   -unfold le_l ; simpl ; intros ; apply False_ind ; assumption.
   -intro v ; destruct v ; unfold le_l ; simpl.
    *trivial.
    *intros J H1 H2.
     elim H1 ; intros H1inf H1rec.
     elim H2 ; intros H2inf H2rec.
     split.
     intro HJ ; apply (Z.le_trans _ z _) ; auto.
     apply (IHt u v (fun n => J (S n))) ; assumption.
 Qed.
(* end hide *)

(** Transitivity.*)
Theorem le_trans :
 forall {n:nat} {J:nat -> Prop} (x y z:vect n),
  x |<=(J) y -> y |<=(J) z -> x |<=(J) z.
Proof.
 intros n J x y z.
 case x,y,z.
 unfold le_vect ; unfold expand_predicate2 ; simpl.
 intros H0 H1 ; exact (le_trans_list x x0 x1 J H0 H1).
Qed.

(* begin hide *)
 Lemma le_refl_list :
  forall (l:list Z) (J:nat -> Prop), le_l J l l.
 Proof.
  induction l.
  unfold le_l ; simpl ; trivial.
  unfold le_l ; simpl.
  split.
  auto with zarith.
  exact (IHl (fun n => J (S n))).
 Qed.
(* end hide *)

(** Reflexivity.*)
Theorem le_refl :
 forall {n:nat} {J:nat -> Prop} (x:vect n), x |<=(J) x.
Proof.
 intros n J.
 induction x.
 unfold le_vect ; unfold expand_predicate2 ; simpl.
 exact (le_refl_list x J).
Qed.

(* begin hide *)
 Lemma le_anti_sym_list :
  forall (t u:list Z) (J:nat -> Prop), le_l J t u -> le_l J u t -> eq_l J t u.
 Proof.
  induction t.
  +induction u.
   -reflexivity.
   -unfold le_l ; simpl ; intros ; apply False_ind ; assumption.
  +intro u ; destruct u.
   -unfold le_l ; simpl ; intros ; apply False_ind ; assumption.
   -unfold le_l ; simpl.
    intros J Htu Hut.
    elim Htu ; intros.
    elim Hut ; intros.
    unfold eq_l ; split.
    intro HJ ; apply (Z.le_antisymm a z) ; [apply H | apply H1] ; assumption.
    apply (IHt u (fun n => J (S n))) ; assumption.
 Qed.
(* end hide *)

(** Antisymetry with respect to partial equality.*)
Theorem le_weak_anti_sym :
 forall {n:nat} {J:nat -> Prop} (x y:vect n),
  x |<=(J) y -> y |<=(J) x -> x |=(J) y.
Proof.
 intros n J x y.
 destruct x,y ; simpl ; intros.
 apply (le_anti_sym_list x x0 J) ; assumption.
Qed.

Corollary le_anti_sym :
 forall {n:nat} (u v:vect n), u |<= v -> v |<= u -> u = v.
Proof.
 intros.
 apply eq_vect_soundness.
 apply le_weak_anti_sym ; assumption.
Qed.

(* begin hide *)
 Lemma le_add_list :
  forall (t u v w:list Z) (J:nat -> Prop),
   le_l J t u -> le_l J v w -> le_l J (addl t v) (addl u w).
 Proof.
  induction t ; intros u v w ; destruct u ; destruct v ; destruct w ;
   unfold addl ; unfold le_l ; simpl ; trivial.
  intros J H0 H1.
  elim H0 ; intros H0inf H0rec.
  elim H1 ; intros H1inf H1rec.
  split.
  intro HJ ; assert ((a <= z /\ z0 <= z1)%Z).
  split ; [apply H0inf | apply H1inf] ; assumption.
  auto with zarith.
  apply (IHt u v w (fun n => J (S n))) ; assumption.
 Qed.
(* end hide *)

(** Compatibility with addition.*)
Theorem le_add :
 forall {n:nat} {J:nat -> Prop} (x y z a:vect n),
   x |<=(J) y -> z |<=(J) a -> (x |+ z) |<=(J) (y |+ a).
Proof.
 intros n J x y z a.
 case x,y,z,a.
 unfold le_vect ; unfold add ; unfold expand_predicate2 ; simpl.
 exact (le_add_list x x0 x1 x2 J).
Qed.

(** Compatibility with addition - weaker version.*)
Theorem le_add_same :
 forall {n:nat} {J:nat -> Prop} (x y c:vect n),
  x |<=(J) y -> x |+ c |<=(J) y |+ c.
Proof.
 intros n J x y c H.
 apply le_add.
 assumption.
 apply (le_refl c).
Qed.
  

(* begin hide *)
Definition positive_list (J:nat -> Prop) :=
 expand_predicate1_list J (fun n => Z.le 0 n).

 Lemma le_positive_soundness_list :
  forall (l:list Z) (J:nat -> Prop),
  positive_list J l <-> le_l J (null_list (length l)) l.
 Proof.
  induction l ; unfold positive_list ; unfold le_l ; simpl.
  +split ; trivial.
  +split ; intro H0 ; elim H0 ; intros Ha Hl ; split ;
   try (apply (IHl (fun n => J (S n)))) ; assumption.
 Qed.
(* end hide *)

(** The positive predicate is consistent with [<=].*)
Theorem le_positive_soundness :
 forall {n:nat} {J:nat -> Prop} (x:vect n), pos_vect J x <-> |0 |<=(J) x.  
Proof.
 intros n J x ; case x.
 intros l e.
 unfold le_vect ; unfold null_vect ; unfold expand_predicate2 ;
 unfold pos_vect ; subst.
 assert (expand_predicate1 J (fun p : Z => (0 <= p)%Z)
  (exist (fun t => length t = length l) l eq_refl) = positive_list J l).
 destruct l ; unfold positive_list ; simpl ; trivial.
 rewrite H.
 apply (le_positive_soundness_list l J).
Qed.


(* begin hide *)
Definition negative_list (J:nat -> Prop) :=
 expand_predicate1_list J (fun n => Z.le n 0).

 Lemma le_negative_soundness_list :
  forall (l:list Z) (J:nat -> Prop),
  negative_list J l <-> le_l J l (null_list (length l)).
 Proof.
  induction l ; unfold positive_list ; unfold le_l ; simpl.
  +split ; trivial.
  +split ; intro H0 ; elim H0 ; intros Ha Hl ; split ;
   try (apply (IHl (fun n => J (S n)))) ; assumption.
 Qed.
(* end hide *)

(** The negative predicate is consistent with [<=].*)
Theorem le_negative_soundness :
 forall {n:nat} {J:nat -> Prop} (x:vect n), neg_vect J x <-> x |<=(J) |0.
Proof.
 intros n J x ; case x.
 intros l e.
 unfold le_vect ; unfold null_vect ; unfold expand_predicate2 ;
 unfold neg_vect ; subst.
 assert (expand_predicate1 J (fun p : Z => (p <= 0)%Z)
  (exist (fun t => length t = length l) l eq_refl) = negative_list J l).
 destruct l ; unfold negative_list ; simpl ; trivial.
 rewrite H.
 simpl.
 apply (le_negative_soundness_list l J).
Qed.

(** Compatibility with opposite.*)
Theorem le_opp :
 forall {n:nat} {J:nat -> Prop} (x y:vect n),
  x |<=(J) y -> |- y |<=(J) |- x.
Proof.
 intros n J x y H.
 rewrite <- (neutral_0_r (|- x)).
 rewrite <- (add_opp_r y).
 rewrite add_assoc.
 rewrite <- (neutral_0_l (|- y)) at 1.
 apply le_add.
 rewrite <- (add_opp_l x).
 apply le_add.
 apply le_refl.
 assumption.
 apply le_refl.
Qed.
 
(** Addition preserves positivity and negativity.*)
Theorem le_add_pos :
  forall {n:nat} {J:nat -> Prop} (x y:vect n),
  pos_vect J x -> pos_vect J y -> pos_vect J (x |+ y).
Proof.
 intros n J x y Hx Hy.
 apply (le_positive_soundness _).
 rewrite <- (neutral_0_r |0).
 apply le_add ; apply le_positive_soundness ; assumption.
Qed.

Theorem le_add_neg :
 forall {n:nat} {J:nat -> Prop} (x y:vect n),
  neg_vect J x -> neg_vect J y -> neg_vect J (x |+ y).
Proof.
 intros n J x y Hx Hy.
 apply le_negative_soundness.
 rewrite <- (neutral_0_r |0).
 apply le_add ; apply le_negative_soundness ; assumption.
Qed.

(* begin hide *)
 Lemma le_list_to_coord :
  forall (p:nat) (t u:list Z) (J:nat -> Prop),
   expand_predicate2_list J Z.le t u -> J p ->
   (coord_list p t <= coord_list p u)%Z.
 Proof.
  induction p.
  +induction t.
   -induction u ; simpl ; intros.
    *apply Z.le_refl.
    *apply False_ind ; assumption.
   -induction u ; unfold coord_list ; simpl.
    *intros ; apply False_ind ; assumption.
    *intros J H HJ ; elim H ; intros Hinf Hrec.
     exact (Hinf HJ).
  +induction t.
   -induction u ; simpl ; intros.
    *apply Z.le_refl.
    *apply False_ind ; assumption.
   -induction u ; unfold coord_list ; simpl.
    *intros ; apply False_ind ; assumption.
    *intros J H HJ ; elim H ; intros.
     apply (IHp t u (fun n => J (S n))) ; assumption.
 Qed.
(* end hide *)

(** Deduce inequality on each component from global inequality.*)
Theorem le_vect_to_coord :
 forall {n:nat} {J:nat -> Prop} (p:nat) (x y:vect n),
  x |<=(J) y -> J p -> (coord p x <= coord p y)%Z.
Proof.
 intros n J p x y Hinf HJ.
 destruct x,y.
 unfold coord ; simpl.
 apply (le_list_to_coord p x x0 J) ; assumption.
Qed.

(* begin hide *)
 Lemma le_coord_to_list :
  forall (t u:list Z) (J:nat -> Prop), length t = length u ->
   (forall p:nat, J p -> (coord_list p t <= coord_list p u)%Z) -> le_l J t u.
 Proof.
  induction t ; destruct u ; unfold le_l ; simpl ;
  try discriminate ; trivial.
  split.
  +intro H1 ; apply (H0 0) ; assumption.
  +refine (IHt u (fun n => J (S n)) _ _).
   injection H ; trivial.
   unfold coord_list ; simpl.
   intro p ; apply (H0 (S p)).
 Qed.
(* end hide *)

(** Deduce global inequality from inequality on each component.*)
Theorem le_coord_to_vect :
 forall {n:nat} {J:nat -> Prop} (x y:vect n),
  (forall p:nat, J p -> (coord p x <= coord p y)%Z) -> x |<=(J) y.
Proof.
 intros n J x y H.
 destruct x,y.
 unfold coord ; simpl.
 apply (le_coord_to_list x x0 J) ; try (rewrite e0) ; assumption.
Qed.

 
(** < is irreflexive*)
Theorem lt_irrefl :
 forall {n:nat} {J:nat -> Prop} (x y:vect n),
  x |<(J) y -> x <> y.
Proof.
 intros n J x y Hlt.
 elim Hlt ; intros _ Hex.
 elim Hex ; intros p Hcoord.
 intro Heq.
 assert(coord p x <> coord p y) ; auto with zarith.
 elim H ; rewrite Heq ; reflexivity.
Qed.

(** Strong transivity on the left.*)
Theorem lt_trans_l :
 forall {n:nat} {J:nat -> Prop} (x y z:vect n),
  x |<=(J) y -> y |<(J) z -> x |<(J) z.
Proof.
 intros n J x y z Hxy Hyz.
 elim Hyz ; intros Hle Hex.
 elim Hex ; intros p Hp.
 elim Hp ; intros HI Hinf.
 split.
 apply (le_trans x y z) ; assumption.
 exists p.
 assert ((coord p x <= coord p y)%Z).
 apply (le_vect_to_coord (J := J)) ; assumption.
 auto with zarith.
Qed.

(** Strong transivity on the right.*)
Theorem lt_trans_r :
 forall {n:nat} {J:nat -> Prop} (x y z:vect n),
  x |<(J) y -> y |<=(J) z -> x |<(J) z.
Proof.
 intros n J x y z Hxy Hyz.
 elim Hxy ; intros Hle Hex.
 elim Hex ; intros p Hp.
 elim Hp ; intros HI Hinf.
 split.
 apply (le_trans x y z) ; assumption.
 exists p.
 assert ((coord p y <= coord p z)%Z).
 apply (le_vect_to_coord (J := J)) ; assumption.
 auto with zarith.
Qed.

(** Assymetry.*)
Theorem lt_asym :
 forall {n:nat} {J:nat -> Prop} (x y:vect n), ~(x |<(J) y /\ y |<(J) x).
Proof.
 intros n J x y H.
 elim H ; intros H0 H1.
 elim H1 ; intros.
 assert (x |<(J) x).
 apply (lt_trans_r x y x) ; assumption.
 elim H4 ; intros.
 elim H6 ; intros p Hp.
 elim Hp ; intros ; auto with zarith.
Qed.
 
(** Transivity.*)
Theorem lt_trans :
 forall {n:nat} {J:nat -> Prop} (x y z:vect n),
  x |<(J) y -> y |<(J) z -> x |<(J) z.
Proof.
 intros n J x y z Hxy Hyz.
 elim Hxy ; intros.
 apply (lt_trans_l x y z) ; assumption.
Qed.
