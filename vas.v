(** VAS module. *)

(* Copyright (C) 2018 Quentin Gougeon

This programm is under GPL license version 3.

See https://www.gnu.org/licenses/gpl.html for details.*)

Require Export TS vector_def.
Require Import vector_group vector_order.

(** * Main definitions*)
Definition cnf {d:nat} := vect d.

Definition is_vas_tr_fun {d:nat} {label : Type}
 (f:cnf -> label -> cnf -> Prop) : Prop :=
 exists (marking:vect d -> option label), forall (c c':cnf) (a:label),
 f c a c' <-> marking (c' |- c) = Some a.

Definition is_vas {d:nat} {label AP:Type} (s:TS cnf label AP) : Prop :=
 is_vas_tr_fun (d := d) (label := label) tr_fun.

Definition mk_vas {d:nat} {label AP:Type} (is_cnf:cnf -> Prop)
 (marking:vect d -> option label) (val:cnf -> AP -> Prop) :
  TS cnf label AP :=
 mkTS cnf label AP is_cnf (fun c a c' => marking (c' |- c) = Some a) val.

Lemma mk_vas_soundness :
 forall {d:nat} {label AP:Type} (is_cnf:cnf -> Prop)
 (marking:vect d -> option label) (val:cnf -> AP -> Prop),
 is_vas (mk_vas is_cnf marking val).
Proof.
 intros d label AP is_cnf marking val.
 try (exists marking) ; simpl ; split ; trivial.
Qed.

(** * Monotony*)
Definition close_sup {d:nat} (J:nat -> Prop) (P:vect d -> Prop) : Prop :=
 forall (x y:vect d), P x -> x |<=(J) y -> P y.

(** Generalized monotony property on one step.*)
Lemma monotonous_1 :
 forall {d:nat} {label AP:Type} (J:nat -> Prop)
  (s:TS cnf label AP) (Hvas:is_vas (label := label) s) (u v:cnf),
  close_sup (d := d) J is_cnf -> reachable_1_from s u v -> 
  forall u':cnf, u |<=(J) u' -> reachable_1_from s u' (v |+ u' |- u).
Proof.
 intros d label AP J s Hvas u v.
 elim Hvas ; intros mark Hmark Hsup Hreach.
 elim Hreach.
 intros Hu H0.
 elim H0 ; intros Hv H1.
 elim H1 ; intros a Htr u' H'.
 +repeat split.
  apply (Hsup u u') ; assumption.
  apply (Hsup v _).
  assumption.
  rewrite <- (neutral_0_r v) at 1.
  rewrite <- (add_opp_r u).
  rewrite (add_assoc v _ _).
  rewrite <- sub_soundness.
  apply le_add_same.
  rewrite (add_comm v u).
  rewrite (add_comm v u').
  apply le_add_same.
  assumption.

  exists a ; apply Hmark.
  repeat (rewrite <- sub_soundness).
  rewrite <- (add_assoc v _ _).
  rewrite (add_comm u' _).
  rewrite add_assoc.
  rewrite <- (add_assoc _ u' _).
  rewrite add_opp_r ; rewrite neutral_0_r.
  rewrite sub_soundness.
  apply Hmark ; assumption.
Qed.

(** Generalized monotony property.*)
Lemma monotonous :
 forall {d:nat} {label AP:Type} (J:nat -> Prop)
  (s:TS cnf label AP) (Hvas:is_vas s) (u v:cnf),
  close_sup (d := d) J is_cnf -> reachable_from s u v -> 
  forall u':cnf, u |<=(J) u' -> reachable_from s u' (v |+ u' |- u).
Proof.
 intros d label AP J s Hvas u v Hsup Hreach.
 elim Hreach.
 intro p ; elim p.
 +intros c Hc u' Hu'.
  rewrite add_comm.
  rewrite <- sub_soundness.
  rewrite <- add_assoc.
  rewrite add_opp_r.
  rewrite neutral_0_r.
  assert(is_cnf u'). exact (Hsup c u' Hc Hu').
  split ; [apply rt1n_refl | assumption].
 +intros c0 c1 c2 H01 H12 Hrec Hc0 u' Hu'.
  elim H01 ; intros _ H ; elim H ; intros Hc1 Hex ; elim Hex ; intros t Ht.
  split.
  apply (rt1n_trans cnf _ u' ((c1 |+ u') |- c0) _).
  -apply (monotonous_1 J) ; assumption.
  -rewrite <- (neutral_0_l u') at 2.
   rewrite <- (add_opp_l c1).
   rewrite <- add_assoc.
   rewrite add_assoc.
   rewrite (add_comm c2 _).
   rewrite <- add_assoc.
   rewrite <- (sub_soundness ((|- c1) |+ (c2 |+ (c1 |+ u'))) c0).
   rewrite (add_comm (|- c1) _).
   rewrite <- add_assoc.
   rewrite (add_comm (|- c1) _).
   rewrite add_assoc.
   rewrite <- (add_assoc _ (c1 |+ u') _).
   repeat rewrite sub_soundness.   
   apply (Hrec Hc1 (c1 |+ u' |- c0)).
   rewrite <- sub_soundness.
   rewrite <- add_assoc.
   rewrite (add_comm c1 _).
   rewrite <- (neutral_0_l c1) at 1.
   apply le_add_same.
   rewrite <- (add_opp_r c0).
   apply le_add_same ; assumption.
  -apply (Hsup c0 u' Hc0 Hu').
Qed.

(** * Executions in VAS*)
(** Since VAS are deterministic, executions can be followed in an unique way.*)
Fixpoint apply_exec {d:nat} (l:exec) : cnf (d:=d) -> cnf :=
 match l with
 |nil => fun c0 => c0
 |t :: l0 => fun c0 => apply_exec l0 (c0 |+ t)
 end.

(** Sum of an execution.*)
Fixpoint sum {d:nat} (l:exec) : cnf (d:=d) :=
 List.fold_left add l |0.