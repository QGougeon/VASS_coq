(** Complete vector module. *)

(* Copyright (C) 2018 Quentin Gougeon

This programm is under GPL license version 3.

See https://www.gnu.org/licenses/gpl.html for details.*)

Require Export vector_group vector_order vector_merge vector_proj.