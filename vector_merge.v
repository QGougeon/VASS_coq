(** Vector merging properties. *)

(* Copyright (C) 2018 Quentin Gougeon

This programm is under GPL license version 3.

See https://www.gnu.org/licenses/gpl.html for details.*)

Require Export vector_def.
Require Import vector_group vector_order List Arith.Compare_dec.

(** * Group-related properties *)
(* begin hide *)
Lemma add_merge_list :
 forall (t u t' u':list Z), length t = length u -> length t' = length u' ->
  (addl t u) ++ (addl t' u') = addl (t ++ t') (u ++ u').
Proof.
 induction t ; intros u t' u' ; destruct u,t',u' ; unfold addl ;
 repeat (rewrite app_nil_r) ; simpl ; try reflexivity ; try discriminate.
 intros H0 H0'.
 rewrite (addl_useful (t ++ _) _).
 rewrite <- (IHt u (z0 :: t') (z1 :: u')).
 reflexivity.
 injection H0 ; trivial.
 simpl ; assumption.
Qed.
(* end hide *)

(** Compatibility of merging with addition.*)
Theorem add_merge :
 forall {n n':nat} (x y:vect n) (x' y':vect n'),
  (x |+ y) |: (x' |+ y') = (x |: x') |+ (y |: y').
Proof.
 intros n n' x y x' y'.
 apply eq_list_implies_eq_vect.
 destruct x,y,x',y' ; simpl.
 apply add_merge_list ; [rewrite e0 | rewrite e2] ; assumption.
Qed.

(* begin hide *)
Lemma opp_merge_list :
 forall (t t':list Z), (oppl t) ++ (oppl t') = oppl (t ++ t').
Proof.
 induction t ; intro t' ; destruct t' ; unfold oppl ;
 repeat (rewrite app_nil_r) ; simpl ; try reflexivity ; try discriminate.
 rewrite (oppl_useful (t ++ _)).
 rewrite <- (IHt (z :: t')) ; reflexivity.
Qed.
(* end hide *)

(** Compatibility of merging with opposite.*)
Theorem opp_merge :
 forall {n n':nat} (x:vect n) (x':vect n'),
  (|- x) |: (|- x') = |- (x |: x').
Proof.
 intros n n' x x'.
 apply eq_list_implies_eq_vect.
 destruct x,x' ; simpl.
 apply opp_merge_list.
Qed.

(** Compatibility of merging with substraction.*)
Theorem sub_merge :
 forall {n n':nat} (x y:vect n) (x' y':vect n'),
  (x |- y) |: (x' |- y') = (x |: x') |- (y |: y').
Proof.
 intros n n' x y x' y'.
 rewrite <- sub_soundness.
 rewrite <- sub_soundness.
 rewrite add_merge.
 rewrite opp_merge.
 apply sub_soundness.
Qed.

(** The merging of two null vectors provides a null vector.*)
Theorem merge_zero :
 forall {n n':nat}, (null_vect (n := n)) |: (null_vect (n := n')) = |0.
Proof.
 intros n n'.
 rewrite <- (sub_inverse ((null_vect (n := n)) |: (null_vect (n := n')))).
 rewrite <- sub_merge.
 repeat (rewrite <- sub_soundness ; rewrite neutral_0_l ; rewrite opp_zero).
 reflexivity.
Qed.

(** * Properties involving the coordinates*)
(* begin hide *)
Lemma merge_coord_up_list :
 forall (p:nat) (t t':list Z),
  p < length t -> coord_list p (t ++ t') = coord_list p t.
Proof.
 induction p ; simpl.
 +intro t ; destruct t ; simpl.
  -assert (~(0 < 0)).
    auto with arith.
   intros ; apply False_ind ; auto.
  -reflexivity.
 +intro t ; destruct t ; unfold coord_list ; simpl.
  -assert (~(S p < 0)).
    auto with arith.
   intros ; apply False_ind ; auto.
  -intros t' Hp.
   apply IHp.
   auto with arith.
Qed.
(* end hide *)

(** The upper coordinates of x [|:] y are y's ones.*)
Theorem merge_coord_up :
 forall {d d':nat} (p:nat) (x:vect d) (x':vect d'),
  p < d -> coord p (x |: x') = coord p x.
Proof.
 intros d d' p x x' Hp ; destruct x,x' ; intros.
 unfold coord ; simpl.
 apply merge_coord_up_list.
 rewrite e ; assumption.
Qed.

(* begin hide *)
Lemma merge_coord_down_list :
 forall (p:nat) (t t':list Z),
  p >= length t -> coord_list p (t ++ t') = coord_list (p - length t) t'.
Proof.
 induction p ; unfold coord_list ; simpl.
 +intros t t' Hp.
  assert (length t = 0).
   auto with arith.
  assert (t = nil).
   apply length_zero_iff_nil ; assumption.
  rewrite H0 ; simpl ; reflexivity.
 +intro t ; destruct t ; simpl.
  -reflexivity.
  -intros t' Hp ; apply IHp ; auto with arith.
Qed.
(* end hide *)

(** The lower coordinates of x [|:] y are y's ones.*)
Theorem merge_coord_down :
 forall {d d':nat} (p:nat) (x:vect d) (x':vect d'),
  p >= d -> coord p (x |: x') = coord (p - d) x'.
Proof.
 intros d d' p x x' Hp ; destruct x,x' ; intros.
 unfold coord ; simpl.
 rewrite <- e.
 apply merge_coord_down_list.
 rewrite e ; assumption.
Qed.

(** * Order-related properties*)
(** Deduce inequality of the merging from inequality of the partials vectors.*)
Theorem merge_le :
 forall {d d':nat} (J J':nat -> Prop) (x y:vect d) (x' y':vect d'),
 x |<=(J) y -> x' |<=(J') y' ->
 (x |: x') |<=(J :[d] J') (y |: y').
Proof.
 intros d d' J J' x y x' y' Hle Hle'.
 apply le_coord_to_vect.
 intros p Hp ; elim Hp.
 +intros H0 ; elim H0 ; intros H1 H2.
  repeat (rewrite merge_coord_up ; try assumption).
  apply (le_vect_to_coord (J := J)) ; assumption.
 +intro H0 ; elim H0 ; intros H1 H2.
  repeat (rewrite merge_coord_down ; try assumption).
  apply (le_vect_to_coord (J := J')) ; assumption.
Qed.

(* begin hide *)
Lemma not_le_implies_le :
 forall (n p:nat), ~ (n < p) -> n >= p.
Proof.
 induction n ; destruct p ; simpl ; intro ; auto with arith.
 +apply False_ind ; elim H ; auto with arith.
 +assert (n >= p).
   apply IHn.
   intro H' ; apply H ; auto with arith.
  auto with arith.
Qed.
(* end hide *)

(** Deduce inequality of the partials vectors from inequality on the merging.*)
Theorem merge_le_rev :
 forall {d d':nat} (J J':nat -> Prop) (x y:vect d) (x' y':vect d'),
 (x |: x') |<=(J :[d] J') (y |: y') -> x |<=(J) y /\ x' |<=(J') y'.
Proof.
 intros d d' J J' x y x' y' Hle ; split.
 +apply le_coord_to_vect.
  intros p Hp ; elim (lt_dec p d).
  -intro H0.
   rewrite <- (merge_coord_up p x x').
   rewrite <- (merge_coord_up p y y').
   apply (le_vect_to_coord (J := J:[d]J')).
   assumption.
   left ; split ; assumption.
   assumption.
   assumption.
  -intro H0.
   repeat (rewrite coord_over) ; try (apply le_not_lt ; assumption).
   auto with zarith.
   apply not_le_implies_le ; assumption.
   apply not_le_implies_le ; assumption.
  +apply le_coord_to_vect.
   intros p Hp.
   rewrite <- (minus_plus d p).
   rewrite <- (merge_coord_down (d + p) x x').
   rewrite <- (merge_coord_down (d + p) y y').
   apply (le_vect_to_coord (J := J:[d]J')).
   assumption.
   right ; split.
   auto with arith.
   rewrite minus_plus ; assumption.
   auto with arith.
   auto with arith.
Qed.


(** Compatiblity of merging with [<] on the upper vector.*)
Theorem merge_lt_up :
 forall {d d':nat} (J J':nat -> Prop) (x y:vect d) (x' y':vect d'),
 x |<(J) y -> x' |<=(J') y' ->
 (x |: x') |<(J :[d] J') (y |: y').
Proof.
 intros d d' J J' x y x' y' Hlt Hle'.
 elim Hlt ; intros Hle Hex ; elim Hex ; intros p Hp ;
  elim Hp ; intros Hp1 Hp2.
 split.
 apply merge_le ; assumption.
 exists p.
 assert ({d <= p} + {d > p}).
  apply le_lt_dec.
 elim H.
 +intro H0 ; apply False_ind.
  assert (~(0 < 0)%Z).
   auto with zarith.
  elim H1.
  assert (coord p x = 0%Z).
   apply coord_over ; assumption.
  assert (coord p y = 0%Z).
   apply coord_over ; assumption.
  rewrite <- H2 at 1 ; rewrite <- H3 ; assumption.
 +intro H0 ; split.
  left ; split ; assumption.
  repeat (rewrite merge_coord_up) ; assumption.
Qed.

(** Compatiblity of merging with [<] on the lower vector.*)
Theorem merge_lt_down :
 forall {d d':nat} (J J':nat -> Prop) (x y:vect d) (x' y':vect d'),
 x |<=(J) y -> x' |<(J') y' ->
 (x |: x') |<(J :[d] J') (y |: y').
Proof.
 intros d d' J J' x y x' y' Hle Hlt'.
 elim Hlt' ; intros Hle' Hex ; elim Hex ; intros p Hp ;
  elim Hp ; intros Hp1 Hp2.
 split.
 apply merge_le ; assumption.
 exists (p + d).
 assert ({d' <= p} + {d' > p}).
  apply le_lt_dec.
 elim H.
 +intro H0 ; apply False_ind.
  assert (~(0 < 0)%Z).
   auto with zarith.
  elim H1.
  assert (coord p x' = 0%Z).
   apply coord_over ; assumption.
  assert (coord p y' = 0%Z).
   apply coord_over ; assumption.
  rewrite <- H2 at 1 ; rewrite <- H3 ; assumption.
 +assert (p + d - d = p).
   auto with zarith.
  intro H1 ; split.
  right ; split.
   auto with zarith.
   rewrite H0 ; assumption.
  repeat (rewrite merge_coord_down) ; auto with arith.
  rewrite H0 ; assumption.
Qed.