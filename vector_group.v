(** Vector addition properties. *)

(* Copyright (C) 2018 Quentin Gougeon

This programm is under GPL license version 3.

See https://www.gnu.org/licenses/gpl.html for details.*)

Require Import List.
Require Export vector_def.

(* begin hide *)
 Definition addl := expand_fun2_list Zplus.
 Definition subl := expand_fun2_list Zminus.
 Definition oppl := expand_fun1_list Zopp.


 Lemma addl_useful :
  forall (t u:list Z), expand_fun2_list Z.add t u = addl t u.
 Proof.
  intros t u.
  unfold addl ; reflexivity.
 Qed.

 Lemma subl_useful :
  forall (t u:list Z), expand_fun2_list Z.sub t u = subl t u.
 Proof.
  intros t u.
  unfold subl ; reflexivity.
 Qed.
 
 Lemma oppl_useful :
  forall (t:list Z), expand_fun1_list Zopp t = oppl t.
 Proof.
  intro t ; unfold oppl ;  reflexivity.
 Qed.

 Lemma add_comm_list :
  forall (t u:list Z), addl t u = addl u t.
 Proof.
  induction t ; induction u ; unfold addl ; simpl ; try reflexivity.
  rewrite (addl_useful t u) ; rewrite (IHt u) ;
  rewrite (Zplus_comm a a0) ; reflexivity.
 Qed.
(* end hide *)

(** Commutativity.*)
Theorem add_comm :
 forall {n:nat} (x y:vect n), x |+ y = y |+ x.
Proof.
 intros n x y.
 apply (eq_list_implies_eq_vect (x |+ y) (y |+ x)).
 induction x.
 induction y.
 simpl.
 exact (add_comm_list x x0).
Qed.

(* begin hide *)
 Lemma add_assoc_list :
  forall (t u v:list Z), addl t (addl u v) = addl (addl t u) v.
 Proof.
  induction t ; induction u ; induction v ; unfold addl ;
  simpl ; try reflexivity.
  rewrite (addl_useful u v).
  rewrite (addl_useful t (addl u v)).
  rewrite (addl_useful t u).
  rewrite (addl_useful (addl t u) v).
  rewrite (IHt u v).
  rewrite (Zplus_assoc a a0 a1).
  reflexivity.
 Qed.
(* end hide *)

(** Associativity.*)
Theorem add_assoc :
 forall {n:nat} (x y z:vect n), x |+ (y |+ z) = (x |+ y) |+ z.
Proof.
 intros n x y z.
 apply (eq_list_implies_eq_vect (x |+ (y |+ z)) ((x |+ y) |+ z)).
 case x,y,z.
 simpl.
 apply (add_assoc_list x x0 x1).
Qed.

(* begin hide *)
 Lemma neutral_0_list :
  forall (t:list Z), addl t (null_list (length t)) = t.
 Proof.
  induction t ; unfold addl ; simpl.
  +reflexivity.
  +rewrite (addl_useful t (null_list (length t))).
   rewrite IHt.
   rewrite Zplus_0_r.
   reflexivity.
 Qed.
(* end hide *)

(** The null vector is neutral on the right.*)
Theorem neutral_0_r :
 forall {n:nat} (x:vect n), x |+ |0 = x.
Proof.
 intros n x.
 apply (eq_list_implies_eq_vect (x |+ |0) x).
 induction x ; simpl.
 rewrite <- p.
 exact (neutral_0_list x).
Qed.

 (** The null vector is neutral on the left.*)
Theorem neutral_0_l :
 forall {n:nat} (x:vect n), |0 |+ x = x.
Proof.
 intros n x.
 rewrite (add_comm |0 x).
 exact (neutral_0_r x).
Qed.

(* begin hide *)
 Lemma add_opp_list :
  forall (t:list Z), addl t (oppl t) = null_list (length t).
 Proof.
  induction t ; unfold addl ; simpl.
  +reflexivity.
  +rewrite (addl_useful t).
   rewrite (oppl_useful t).
   rewrite IHt.
   rewrite (Zplus_opp_r a).
   reflexivity.
 Qed.
(* end hide *)

(** [-x] is the inverse of [x] on the right.*)
Theorem add_opp_r :
 forall {n:nat} (x:vect n), x |+ (|- x) = |0.
Proof.
 intros n x.
 apply (eq_list_implies_eq_vect (x |+ (|- x)) |0).
 induction x.
 simpl.
 rewrite <- p.
 exact (add_opp_list x).
Qed.

(** [-x] is the inverse of [x] on the left.*)
Theorem add_opp_l :
 forall {n:nat} (x:vect n),(|- x) |+ x = |0.
Proof.
 intros n x.
 rewrite (add_comm (|- x) x).
 exact (add_opp_r x).
Qed.

(* begin hide *)
 Lemma sub_soundness_list :
  forall (t u:list Z), addl t (oppl u) = subl t u.
 Proof.
  induction t ; induction u ; unfold subl ; unfold addl ;
  simpl ; try reflexivity.
  rewrite (subl_useful t u).
  rewrite (oppl_useful u).
  rewrite (addl_useful t (oppl u)).
  rewrite (IHt u).
  assert ((a - a0 = a + (- a0))%Z).
  auto with zarith.
  rewrite H ; reflexivity.
 Qed.
(* end hide *)

(** Using the notation [-] 
 for both the unary and binary operators is consistent.*)
Theorem sub_soundness :
 forall {n:nat} (x y:vect n), x |+ (|- y) = x |- y.
Proof.
 intros n x y.
 apply (eq_list_implies_eq_vect (x |+ (|- y)) (x |- y)).
 induction x.
 induction y.
 simpl.
 exact (sub_soundness_list x x0).
Qed.

Corollary sub_inverse :
 forall {n:nat} (x:vect n), x |- x = |0.
Proof.
 intros n x.
 rewrite <- (sub_soundness x).
 apply add_opp_r.
Qed.

(** The opposite operator is involutive.*)
Theorem opp_opp :
 forall {n:nat} (x:vect n), |- (|- x) = x.
Proof.
 intros n x.
 rewrite <- (neutral_0_l (|- (|- x))).
 rewrite <- (add_opp_r x).
 rewrite <- add_assoc.
 rewrite add_opp_r .
 exact (neutral_0_r x). 
Qed.

 (** The opposite of zero is zero.*)
Theorem opp_zero :
 forall {n:nat}, |- (null_vect (n:=n)) = |0.
Proof.
 intro n.
 apply eq_list_implies_eq_vect.
 simpl.
 induction n ; simpl.
 +reflexivity.
 +rewrite IHn ; reflexivity.
Qed.

(* begin hide *)
 Lemma coord_over_list :
  forall (p:nat) (t:list Z), p >= length t -> coord_list p t = 0%Z.
 Proof.
 induction p ; simpl.
  +intros t Ht.
   assert (t = nil).
    apply length_zero_iff_nil.
    auto with arith.
   rewrite H ; simpl ; reflexivity.
  +destruct t.
   -simpl ; reflexivity.
   -unfold coord_list ; simpl ; intro Ht.
    apply IHp ; auto with arith.
 Qed.
(* end hide *)

(** Components of index greater than the length of the vector are null.*)
Theorem coord_over :
 forall {n:nat} (p:nat) (x:vect n),
  p >= n -> coord p x = 0%Z.
Proof.
 intros n p ; destruct x.
 unfold coord ; simpl.
 intro H ; apply coord_over_list.
 rewrite e ; assumption.
Qed.
   
(* begin hide *)
 Lemma add_coord_list :
  forall (p:nat) (t u:list Z), length t = length u ->
   coord_list p (addl t u) = ((coord_list p t) + (coord_list p u))%Z.
 Proof.
  induction p ; induction t ; induction u ; simpl ; try reflexivity ;
  try discriminate ; try (auto with zarith).
  intro Hlength.
  refine (IHp t u _).
  injection Hlength.
  intro ; assumption.
 Qed.
(* end hide *)

(** Coord is a morphism.*)
Theorem add_coord :
 forall {n:nat} (p:nat) (x y:vect n),
  coord p (x |+ y) = (coord p x + coord p y)%Z.
Proof.
 intros n p x y.
 destruct x,y.
 unfold coord.
 unfold add.
 simpl.
 apply add_coord_list.
 rewrite e ; rewrite e0 ; reflexivity.
Qed.
 
(** Other morphism properties.*)
Theorem coord_zero :
 forall {n:nat} (p:nat), coord (n := n) p |0 = 0%Z.
Proof.
 intros n p.
 assert (0%Z = ((coord (n := n) p |0) - (coord (n := n) p |0))%Z).
 auto with zarith.
 rewrite H.  
 rewrite <- (neutral_0_l |0) at 2.
 rewrite add_coord.
 auto with zarith.
Qed.

Theorem opp_coord :
 forall {n:nat} (p:nat) (x:vect n),
  coord p (|- x) = (- coord p x)%Z.
Proof.
 intros n p x.
 assert((coord p (|- x) + coord p x = 0)%Z).
 rewrite <- add_coord.
 rewrite add_opp_l.
 apply coord_zero.
 auto with zarith.
Qed.

Theorem sub_coord :
 forall {n:nat} (p:nat) (x y:vect n),
  coord p (x |- y) = (coord p x - coord p y)%Z.
Proof.
 intros n p x y.
 rewrite <- sub_soundness.
 rewrite add_coord.
 rewrite opp_coord.
 auto with zarith.
Qed.
