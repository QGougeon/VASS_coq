(** Transition systems module. *)

(* Copyright (C) 2018 Quentin Gougeon

This programm is under GPL license version 3.

See https://www.gnu.org/licenses/gpl.html for details.*)

Require Export List Relation_Operators.

Class TS (cnf label AP:Type) :=
 mkTS {is_cnf : cnf -> Prop ; tr_fun : cnf -> label -> cnf -> Prop ;
       val : cnf -> AP -> Prop}.


(** * Paths and executions*)
 (** ** Paths*)
 (** A path is a sequence of configurations whose each element can
   be reached from its predecessor through a transition.*)
 Inductive path {cnf label:Type}
  {tr_fun:cnf -> label -> cnf -> Prop} : cnf -> cnf -> Type :=
  |single_path : forall c:cnf, path c c
  |step_path : forall (c0:cnf) {c1 c2:cnf} (p:path c1 c2) (a:label),
   tr_fun c0 a c1 -> path c0 c2.

 (** Functions that return the origin and the destination of a path ; may be
   useful in case it is unknown because of implicit arguments.*)
 Definition origin {cnf label:Type} {tr_fun:cnf -> label -> cnf -> Prop}
  {c0 c1:cnf} (p:path (label := label) (tr_fun := tr_fun) c0 c1) : cnf := c0.

 Definition dest {cnf label:Type} {tr_fun:cnf -> label -> cnf -> Prop}
  {c0 c1:cnf} (p:path (label := label) (tr_fun := tr_fun) c0 c1) : cnf := c1.

 (** Predicate that tells whether a path spans across a transition system.*)
 Inductive is_path_in_TS {cnf label AP:Type} (s:TS cnf label AP) :
  forall {c0 c1:cnf}, path (tr_fun := tr_fun) c0 c1 -> Prop :=
  |single_in_TS : forall (c:cnf), is_cnf c -> is_path_in_TS s (single_path c)
  |concat_in_TS : forall (c0 c1 c2:cnf) (p:path c1 c2) (a:label)
   (Ha:tr_fun c0 a c1), is_cnf c0 -> is_path_in_TS s p ->
   is_path_in_TS s (step_path c0 p a Ha).

 (** The origin and the destination of a path are valid configurations.*)
 Lemma path_TS_soundness :
  forall {cnf label AP:Type} (s:TS cnf label AP)
  {c0 c1:cnf} (p : path (tr_fun := tr_fun) c0 c1),
  is_path_in_TS s p -> is_cnf c0  /\ is_cnf c1.
 Proof.
  intros cnf label AP s c0 c1 p Hp.
  elim Hp ; intros ; split ; try assumption.
  elim H1 ; intros ; assumption.
 Qed.
 
 (** ** Reachibility*)
  (**Reachability relation over the configurations.*)
 Definition reachable_1_from {cnf label AP:Type}
  (s:TS cnf label AP) (c c':cnf) : Prop :=
  is_cnf c /\ is_cnf c' /\ (exists (a:label), tr_fun c a c').

 Definition reachable_from {cnf label AP:Type} (s:TS cnf label AP) (c c':cnf) :=
  clos_refl_trans_1n cnf (reachable_1_from s) c c' /\ is_cnf c.

 (** Any reachable configuration in a transition system is valid.*)
 Theorem reachibility_soundness :
  forall {cnf label AP:Type} (s:TS cnf label AP)
  (c c':cnf), reachable_from s c c' -> is_cnf c'.
 Proof.
  intros cnf label AP s c c' Hreach ; elim Hreach ; intros Hr ; elim Hr.
  +trivial.
  +intros x y z Hmv Hr' Hyz Hx.
   apply Hyz ; elim Hmv.
   intros _ H ; elim H ; intros ; assumption.
 Qed.  

 (** A configuration is reachable from an other if and only there is a path
   that connects them.*)
 Theorem reachable_iff_expath :
  forall {cnf label AP:Type} (s:TS cnf label AP) (c c':cnf),
  (exists p:path c c', is_path_in_TS s p) <-> reachable_from s c c'.
 Proof.
  intros cnf label AP s c c' ; split.
  +intro H ; elim H ; induction x.
   -intro H' ; split.
    apply (rt1n_refl cnf _ c ).
    apply (path_TS_soundness s (single_path c) H').
   -intro H' ; elim H'.
    intros c Hc ; split ; try (apply (rt1n_refl cnf _ c)) ; assumption.
    intros c3 c4 c5 p b Hmv Hc3 Hpath Hreach.
    split.
    elim Hreach ; intros Hcnf Hr. 
    apply (rt1n_trans cnf _ c3 c4 c5).
    split ; [assumption | split].
    apply (path_TS_soundness s p Hpath).
    exists b ; assumption.
    assumption.
    assumption.
  +intro H ; elim H.
   intros Hreach ; elim Hreach.
   -intro x ; exists (single_path x) ; apply single_in_TS ; assumption.
   -intros x y z Hxy ; elim Hxy.
    intros Hx H0 Hyz Hpath _.
    elim H0 ; intros Hy Hex ; elim Hex ; intros t Hmv.
    assert (exists p : path y z, is_path_in_TS s p). exact (Hpath Hy).
    elim H1 ; intros p Hp.
    exists (step_path x p t Hmv).
    apply concat_in_TS ; assumption.
 Qed.

 (** ** Path concatenation.*)
 (** An operation that merges two paths into one.*)
 Fixpoint concat_path {cnf label:Type} {tr_fun:cnf -> label -> cnf -> Prop}
  {c0 c1 c2:cnf} (p : path (label := label) (tr_fun := tr_fun) c0 c1) :
  path (label := label) (tr_fun := tr_fun) c1 c2 ->
  path (label := label) (tr_fun := tr_fun) c0 c2 :=
 match p with
 |single_path c => fun q => q
 |step_path x p' a Ha =>
  fun q => step_path x (concat_path p' q) a Ha
 end.

 Theorem concat_path_soundness :
  forall {cnf label AP:Type} {c0 c1 c2:cnf} (s:TS cnf label AP)
  (p:path c0 c1), is_path_in_TS s p -> forall (q:path c1 c2),
  is_path_in_TS s q -> is_path_in_TS s (concat_path p q).
 Proof.
  intros cnf label AP c0 c1 c2 s p Hp ; elim Hp ; simpl.
  +trivial.
  +intros x y z q a Ha Hx Hq Hrec t Ht.
   apply concat_in_TS ; try assumption.
   apply Hrec ; assumption.
 Qed.

 Corollary reachability_trans :
  forall {cnf label AP:Type} (s:TS cnf label AP) (c0 c1 c2:cnf),
  reachable_from s c0 c1 -> reachable_from s c1 c2 -> reachable_from s c0 c2.
 Proof. 
  intros.
  assert (exists (p:path c0 c1), is_path_in_TS s p).
  apply reachable_iff_expath ; assumption.
  elim H1 ; intros p Hp.
  assert (exists (p:path c1 c2), is_path_in_TS s p).
  apply reachable_iff_expath ; assumption.
  elim H2 ; intros q Hq.
  apply reachable_iff_expath.
  exists (concat_path p q).
  apply concat_path_soundness ; assumption.
 Qed.

 (** ** Path extension on the right*)
 (** An operation that extends a path by adding a transition at the end.*)
 Fixpoint step_path_right {cnf label:Type} {tr_fun:cnf -> label -> cnf -> Prop}
  {c0 c1 c2:cnf} (p : path c0 c1) (a:label) : tr_fun c1 a c2 -> path c0 c2 :=
 match p with
 |single_path c => fun Ha => step_path c (single_path c2) a Ha
 |step_path x p' b Hb =>
  fun Ha => step_path x (step_path_right p' a Ha) b Hb
 end.

 Theorem step_path_right_soundness :
  forall {cnf label AP:Type} (s:TS cnf label AP)
  {c0 c1 c2:cnf} (p:path c0 c1), is_path_in_TS s p ->
  forall (a:label) (Ha:tr_fun c1 a c2), is_cnf c2 ->
  is_path_in_TS s (step_path_right p a Ha).
 Proof.
  intros cnf label AP s c0 c1 c2 p Hp ; elim Hp.
  +intros ; simpl.
   apply concat_in_TS ; try assumption.
   apply single_in_TS ; assumption.
  +intros x y z pyz b Hb Hx Hpyz Hrec b' Hb' Hy ; simpl.
   apply concat_in_TS ; try assumption.
   apply Hrec ; assumption.
 Qed.
 
 Corollary reachability_right :
  forall {cnf label AP:Type} (s:TS cnf label AP) (c0 c1 c2:cnf) (a:label),
  reachable_from s c0 c1 -> tr_fun c1 a c2 -> is_cnf c2 ->
  reachable_from s c0 c2.
 Proof.
  intros cnf label AP s c0 c1 c2 a H01 Ha Hc2.
  assert (exists (p:path c0 c1), is_path_in_TS s p).
  apply reachable_iff_expath ; assumption.
  elim H ; intros p Hp.
  apply reachable_iff_expath.
  exists (step_path_right p a Ha).
  apply step_path_right_soundness ; assumption.
 Qed.

 (** ** Length*)
 (** The length of a path is consistent with the above operations.*)
 Fixpoint length_path {cnf label:Type} {tr_fun:cnf -> label -> cnf -> Prop}
  {c0 c1:cnf} (p:path (label := label) (tr_fun := tr_fun) c0 c1) : nat :=
  match p with
  |single_path _ => 0
  |step_path _ q _ _ => S (length_path q)
  end.

 Lemma length_concat :
  forall {cnf label:Type} {tr_fun:cnf -> label -> cnf -> Prop}
  {c0 c1 c2:cnf} (p:path c0 c1) (q:path c1 c2),
  length_path (concat_path p q) =
   length_path p + length_path (label := label) (tr_fun := tr_fun) q.
 Proof.
  intros cnf label tr_fun c0 c1 c2 ; induction p.
  +intro ; simpl ; reflexivity.
  +intro q ; simpl ; rewrite (IHp q) ; reflexivity.
 Qed.

 Lemma length_right :
  forall {cnf label:Type} {tr_fun:cnf -> label -> cnf -> Prop}
  {c0 c1 c2:cnf} (p:path c0 c1) (a:label) (Ha:tr_fun c1 a c2),
  length_path (step_path_right p a Ha) = S (length_path p).
 Proof.
  intros cnf label tr_fun c0 c1 c2 p ; induction p.
  +intros ; simpl ; reflexivity.
  +intros ; simpl.
   rewrite (IHp a0 Ha) ; reflexivity.
 Qed.

 (** * Executions*)
 Definition exec {label:Type} := list label.

 (** The trace of a path is its associated execution.*)
 Fixpoint trace {cnf label:Type} {tr_fun:cnf -> label -> cnf -> Prop}
  {c0 c1:cnf} (p:path (tr_fun := tr_fun) c0 c1) : exec :=
  match p with
  |single_path _ => nil
  |step_path _ q a _ =>
   a :: (trace (label := label) (tr_fun := tr_fun) q)
  end.

 (** The trace function is a morphism with respect to the concatenation
  operations.*)
 Theorem trace_concat :
  forall {cnf label:Type} {tr_fun:cnf -> label -> cnf -> Prop}
  {c0 c1 c2:cnf} (p:path c0 c1) (q:path c1 c2),
  trace (concat_path p q) =
   (trace p) ++ (trace (label := label) (tr_fun := tr_fun) q).
 Proof.
  intros cnf label tr_fun c0 c1 c2 ; induction p ; simpl.
  +reflexivity.
  +intro q ; rewrite (IHp q) ; reflexivity.
 Qed.
 
 (** Right-extension adds a transition at the end of the trace.*)
 Theorem trace_right :
  forall {cnf label:Type} {tr_fun:cnf -> label -> cnf -> Prop}
  {c0 c1 c2:cnf} (p:path c0 c1) (a:label) (Ha:tr_fun c1 a c2),
  trace (step_path_right p a Ha) = (trace p) ++ (a :: nil).
 Proof.
  intros cnf label tr_fun c0 c1 c2 ; induction p ; simpl.
  +reflexivity.
  +intros b Hb ; rewrite (IHp b Hb) ; reflexivity.
 Qed.

(** * Bisimulations*)
 (** A bisimulation is a relation between the configurations of two TS
  that preserves the valuation and the behaviour.*)
 Definition is_bisimulation {cnf cnf' label AP:Type}
  (s:TS cnf label AP) (s':TS cnf' label AP) (R:cnf -> cnf' -> Prop) : Prop :=
  (forall (c d:cnf) (c':cnf') (a:label), R c c' -> tr_fun c a d ->
  exists (d':cnf'), R d d' /\ tr_fun c' a d')  (*Forth*)
  /\ (forall (c' d':cnf') (c:cnf) (a:label), R c c' -> tr_fun c' a d' ->
  exists (d:cnf), R d d' /\ tr_fun c a d)  (*Back*)
  /\ (forall (c:cnf) (c':cnf'), R c c' ->
   (is_cnf c <-> is_cnf c') /\ (forall p:AP, val c p <-> val c' p)).  (*Prop*)

 (** Two systems are bisimilar if there exists a bisimulation between them.*)
 Definition bisimilar {cnf cnf' label AP:Type}
  (s:TS cnf label AP) (c0:cnf) (s':TS cnf' label AP) (c'0:cnf') : Prop :=
  exists R:cnf -> cnf' -> Prop, is_bisimulation s s' R /\ R c0 c'0.

 (** Bisimilarity is reflexive.*)
 Theorem bisimilar_refl :
  forall {cnf label AP:Type} (s:TS cnf label AP) (c0:cnf),
   bisimilar s c0 s c0.
 Proof.
  intros cnf label AP s c0.
  exists (fun c c' => c = c') ; split.
  split.
  intros c d c' a Heq Htr ; exists d.
  split ; [reflexivity | rewrite <- Heq ; assumption].
  split.
  intros c d c' a Heq Htr ; exists d.
  split ; [reflexivity | rewrite Heq ; assumption].
  intros c c' Heq ; rewrite Heq ; repeat split ; trivial.
  reflexivity.
 Qed.

 (** Bisimilarity is symetric.*)
 Theorem bisimilar_sym :
  forall {cnf cnf' label AP:Type}
   (s:TS cnf label AP) (c0:cnf) (s':TS cnf' label AP) (c'0:cnf'),
   bisimilar s c0 s' c'0 -> bisimilar s' c'0 s c0.
 Proof.
  intros cnf cnf' label AP s c0 s' c'0 Hb.
  elim Hb ; intros R H ; elim H ; intros H' H0.
  elim H' ; intros Hforth H''.
  elim H'' ; intros Hback Hprop.
  exists (fun c' c => R c c').
  split ; try split ; try assumption.
  intros ; repeat split ; try (apply Hprop) ; assumption.
 Qed.

 (** Bisimilarity is transitive.*)
 Theorem bisimilar_trans :
  forall {cnf cnf' cnf'' label AP:Type}
   (s:TS cnf label AP) (c0:cnf)
   (s':TS cnf' label AP) (c'0:cnf')
   (s'':TS cnf'' label AP) (c''0:cnf''),
   bisimilar s c0 s' c'0 -> bisimilar s' c'0 s'' c''0 ->
    bisimilar s c0 s'' c''0.
 Proof.
  intros cnf cnf' cnf'' label AP s c0 s' c'0 s'' c''0 Hb1 Hb2.
  elim Hb1 ; intros R1 H1 ; elim H1 ; intros H1' H0.
  elim H1' ; intros Hforth1 H1'' ; elim H1'' ; intros Hback1 Hprop1.
  elim Hb2 ; intros R2 H2 ; elim H2 ; intros H2' H0'.
  elim H2' ; intros Hforth2 H2'' ; elim H2'' ; intros Hback2 Hprop2.
  exists (fun c c'' => exists c':cnf', R1 c c' /\ R2 c' c'').
  split ; try split ; try split.
  
  (*Back*)
  +intros c d c'' a HR1 Htr ; elim HR1 ; intros c' Hc'.
   elim Hc' ; intros Hc'1 Hc'2.
   elim (Hforth1 c d c' a Hc'1 Htr) ; intros d' Hd'.
   elim Hd' ; intros Hd'1 Htr'.
   elim (Hforth2 c' d' c'' a Hc'2 Htr') ; intros d'' Hd''.
   elim Hd'' ; intros Hd'2 Htr''.
   exists d'' ; split ; [exists d' ; split | idtac] ; assumption.

  (*Forth*)
  +intros c'' d'' c a HR2 Htr'' ; elim HR2 ; intros c' Hc'.
   elim Hc' ; intros Hc'1 Hc'2.
   elim (Hback2 c'' d'' c' a Hc'2 Htr'') ; intros d' Hd'.
   elim Hd' ; intros Hd'2 Htr'.
   elim (Hback1 c' d' c a Hc'1 Htr') ; intros d Hd.
   elim Hd ; intros Hd1 Htr.
   exists d ; split ; [exists d' ; split | idtac] ; assumption.

  (*Prop*)
  +intros c c'' HR.
   elim HR ; intros c' Hc' ; elim Hc' ; intros HR1 HR2.
   repeat split ; intros ;
   try (apply (Hprop2 c' c'' HR2) ; apply (Hprop1 c c')) ;
   try assumption ;
   (apply (Hprop1 c c' HR1) ; apply (Hprop2 c' c'')) ;
   assumption.

  (*R links c0 and c''0*)
  +exists c'0 ; split ; assumption.
 Qed.

 (** If a state is reachable in a transition, it is reachable in any
  bisimilar transition system.*)
 Theorem bisimulation_preserves_reachability :
  forall {cnf cnf' label AP:Type} (s:TS cnf label AP) (s':TS cnf' label AP)
   (R:cnf -> cnf' -> Prop), is_bisimulation s s' R ->
  forall (c d:cnf), reachable_from s c d -> forall c':cnf',
   R c c' -> exists d':cnf', R d d' /\ reachable_from s' c' d'.
 Proof.
  intros cnf cnf' label AP s s' R Hb c d Hreach ; elim Hreach.
  elim Hb ; intros Hforth H0 ; elim H0 ; intros Hback Hprop.
  intro Hreach' ; elim Hreach'.
  +intros x Hx c' Hxc' ; exists c' ; repeat split.
   assumption.
   apply rt1n_refl.
   apply (Hprop x c') ; assumption.
  +intros x y z Hxy Hyz Hind Hx c' Rxc' ; elim Hxy.
   intros _ H1 ; elim H1 ; intros Hy Hex ; elim Hex ; intros a Htr.
   assert (exists y':cnf', R y y' /\ tr_fun c' a y').
    apply (Hforth x y c' a) ; assumption.
   elim H ; intros y' H2 ; elim H2 ; intros Hyy' Htr'.
   assert (exists d':cnf', R z d' /\ reachable_from s' y' d').
    apply (Hind Hy y') ; assumption.
   elim H3 ; intros d' H4 ; elim H4 ; intros Hzd' H5.
   elim H5 ; intros.
   exists d' ; repeat split.
   assumption.
   apply (rt1n_trans cnf' _ c' y' d') ; repeat split.
   apply (Hprop x c') ; assumption.
   apply (Hprop y y') ; assumption.
   exists a ; assumption.
   assumption.
   apply (Hprop x c') ; assumption.
 Qed.
