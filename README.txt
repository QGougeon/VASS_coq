Author: Quentin Gougeon
Contact : first.last[at]ens-rennes.fr
Description: This project is an embryo of a library in Coq to formally verify proofs on
vector addition systems (VAS) and vector addition systems with states (VASS).
So far most of the efforts have been focused on writing a complete and user-friendly vector module.
Moreover VAS and VASS are regarded as transition systems, which is a higher abstraction.
The purpose of this is to show that a VASS and its VAS conversion (see below) are bisimilar.
This result is true but remains to be written in Coq; once proven,
it would allow to reduce some problems on VASS to similar ones on VAS.

This project is the result of the internship I did at
LaBRI, Bordeaux, France <https://www.labri.fr/>.
Thus you will also find my internship report (rapport.pdf).
It is written in French but I have translated a part of it in English in order to help
further developpment (explanations.pdf).

This program is free software: you can redistribute it or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.
See <https://www.gnu.org/licenses/gpl.html> for more details.

About Coq files:
- vector_def.v contains all the vector-related definitions.
- vector_group.v contains properties regarding plus, minus and opposite operations.
- vector_order.v contains properties regarding the <= and < relations.
- vector_merge.v is devoted to vector merging.
- vector_proj.v is devoted to projections.
- The single purpose of vector_all.v is to directly import the last five libraries.
- TS.v is about transition systems and bisimulations.
- vas.v contains the definition of VAS using transition systems, as well as a proof
  that reachability is monotonous.
- vass.v contains the definition of VASS using transition systems.
- vass_to_vas.v contains the conversion of a VASS into a VAS using a similar method
  to Hopcroft & Pansiot's one (1976).

How to compile:
 Type ./make_make to generate the Makefile.

How to get the documentation:
 Type "sh make_doc.sh" to generate it. A folder named "documentation" will be created.


