(** VASS conversion to VAS. *)

(* Copyright (C) 2018 Quentin Gougeon

This programm is under GPL license version 3.

See https://www.gnu.org/licenses/gpl.html for details.*)

Require Export TS vas vass vector_all.

(** Encoding a VASS configuration with a vector.*)
Definition convert_cnf {d:nat} (N:Z) (c:cnfs (d:=d) N) : vect (d+4) :=
 match (state c) with
 |exist _ q _ => (value c) |: (vect_from_list (
  q :: (N - q + 1) :: 0 :: 0 :: nil)%Z)
 end.

Definition convert_cnf' {d:nat} (N:Z) (c:cnfs (d:=d) N) : vect (d+4) :=
 match (state c) with
 |exist _ q _ => (value c) |: (vect_from_list (
  (0 :: 0 :: q :: (N - q + 1) :: nil)%Z))
 end.

(** Encoding a VASS transition with a vector.*)
Definition convert_tr {d:nat} (N:Z) (c0 c1:cnfs (d:=d) N) : vect (d+4) :=
 match state c0, state c1 with
 |exist _ q _, exist _ r _ => ((value c1) |- (value c0)) |: (vect_from_list (
  -q :: (q - N - 1) :: r :: (N + 1 - r) :: nil)%Z)
 end.

Definition convert_tr' {d:nat} (N:Z) (c0 c1:cnfs (d:=d) N) : vect (d+4) :=
 match state c0, state c1 with
 |exist _ q _, exist _ r _ => ((value c1) |- (value c0)) |: (vect_from_list (
  q :: (N + 1 - q) :: -r :: r - N - 1 :: nil)%Z)
 end.

(** Converting a VASS marking to a VAS marking with respect to
  transition encoding.*)
  Lemma opp_cnf :
   forall (N q:Z), (q <= -1 -> -N <= q -> 1 <= -q <= N)%Z.
  Proof.
   intros ; split ; auto with zarith.
  Qed.

 Definition get_move {d:nat} (u:vect (d + 4)) : vect d :=
 (proj_up d (nat_useful_1 d 4) u).

 Definition convert_marking {d:nat} {label:Type} (N:Z)
 (marking : Q N -> vect d -> Q N -> option label) (u:vect (d+4)) :
  option label :=
  let a0 := coord d u in
  let a1 := coord (d + 1) u in
  let a2 := coord (d + 2) u in
  let a3 := coord (d + 3) u in

  match Z_le_dec a0 (-1), Z_le_dec (-N) a0, Z_eq_dec a1 (a0 - N - 1)%Z,
        Z_le_dec 1 a2, Z_le_dec a2 N, Z_eq_dec a3 (N + 1 - a2) with
  |left H1, left H1', left _, left H3, left H3', left _ =>
   marking (exist _ (-a0)%Z (opp_cnf N a0 H1 H1'))
   (get_move u) (exist _ a2 (conj H3 H3'))
  |_,_,_,_,_,_ =>
   match Z_le_dec 1 a0, Z_le_dec a0 N, Z_eq_dec a1 (N + 1 - a0),
         Z_le_dec a2 (-1), Z_le_dec (-N) a2, Z_eq_dec a3 (a2 - N - 1) with
    |left H1, left H1', left _, left H3, left H3', left _ =>
     marking (exist _ a0 (conj H1 H1'))
     (get_move u) (exist _ (-a2)%Z (opp_cnf N a2 H3 H3'))
    |_,_,_,_,_,_ => None
   end
  end.

(** Converting a VASS cnf predicate to a VAS cnf predicate with respect to
 configuration encoding.*)
Definition convert_is_cnf {d:nat} (N:Z) (is_cnfs:cnfs N -> Prop)
 (u:vect (d+4)) : Prop :=
 let a0 := coord d u in
 let a1 := coord (d + 1) u in
 let a2 := coord (d + 2) u in
 let a3 := coord (d + 3) u in

 match Z_le_dec 1 a0, Z_le_dec a0 N, Z_eq_dec a1 (N + 1 - a0)%Z,
       Z_eq_dec a2 0, Z_eq_dec a3 0 with
  |left H1, left H1', left _, left _, left _ =>
   is_cnfs (exist _ a0 (conj H1 H1'), get_move u)
  |_,_,_,_,_ =>
   match Z_eq_dec a0 0, Z_eq_dec a1 0,
         Z_le_dec 1 a2, Z_le_dec a2 N, Z_eq_dec a3 (N + 1 - a2)%Z with
   |left _, left _, left H3, left H3', left _ =>
    is_cnfs (exist _ a2 (conj H3 H3'), get_move u)
   |_,_,_,_,_ => False
   end
 end.

(** Converting a VASS valuation to a VAS valuation with respect to
 configuration encoding.*)
Definition convert_val {d:nat} {AP:Type} (N:Z) (val:cnfs N -> AP -> Prop)
 (u:vect (d+4)) (p:AP) : Prop :=
 let a0 := coord d u in
 let a1 := coord (d + 1) u in
 let a2 := coord (d + 2) u in
 let a3 := coord (d + 3) u in

 match Z_le_dec 1 a0, Z_le_dec a0 N, Z_eq_dec a1 (N + 1 - a0)%Z,
       Z_eq_dec a2 0, Z_eq_dec a3 0 with
  |left H1, left H1', left _, left _, left _ =>
   val (exist _ a0 (conj H1 H1'), get_move u) p
  |_,_,_,_,_ =>
   match Z_eq_dec a0 0, Z_eq_dec a1 0,
         Z_le_dec 1 a2, Z_le_dec a2 N, Z_eq_dec a3 (N + 1 - a2)%Z with
   |left _, left _, left H3, left H3', left _ =>
    val (exist _ a2 (conj H3 H3'), get_move u) p
   |_,_,_,_,_ => False
   end
 end.

(** Converting a VASS to a VAS by gathering all of the above conversions.*)
Definition convert_vass {d:nat} {label AP:Type} (N:Z)
 (is_cnfs:cnfs (d:=d) N -> Prop) (marking:Q N -> vect d -> Q N -> option label)
 (val:cnfs (d:=d) N -> AP -> Prop) : TS (vect (d+4)) label AP :=
 mk_vas (convert_is_cnf N is_cnfs) (convert_marking N marking)
 (convert_val N val).

(** The relation that links a VASS configuration to its encodings.*)
Definition rel {d:nat} (N:Z) (c:cnfs N) (u:vect (d+4)) : Prop :=
 u = convert_cnf N c \/ u = convert_cnf' N c.

