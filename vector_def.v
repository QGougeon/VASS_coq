(** Main vector-related definitions. *)

(* Copyright (C) 2018 Quentin Gougeon

This programm is under GPL license version 3.

See https://www.gnu.org/licenses/gpl.html for details.*)

Require Import Logic.Eqdep_dec List.
Require Export Arith ZArith.

(** * Main definitions*)
Definition vect (n:nat) := {l:list Z | length l = n}.

Definition vect_from_list (l:list Z) : vect (length l) :=
 exist _ l (eq_refl (length l)).

Definition get_list {n:nat} (x:vect n) : list Z := proj1_sig x.

(** * Null vector*)
Fixpoint null_list (n:nat) : list Z :=
 match n with
 |0 => nil
 |S p => 0%Z :: (null_list p)
 end.

Lemma length_null_list : 
 forall (n:nat), length (null_list n) = n.
Proof.
 induction n.
 simpl ; reflexivity.
 simpl ; rewrite IHn ; reflexivity.
Qed.

Definition null_vect {n:nat} : vect n :=
 exist (fun l => length l = n) (null_list n) (length_null_list n).

Notation "|0" := null_vect.

(** * Coordinates*)
(** Returns the projection on the pth coordinate if 0 <= p < n,
   and 0 otherwise.*)
Definition coord_list (p:nat) (t:list Z) : Z := List.nth p t 0%Z.

Definition coord {n:nat} (p:nat) (x:vect n) : Z :=
 coord_list p (get_list x).


(** * Operations *)
(** ** Unary operations*)
(** Infer a [vect n -> vect n] type function from a [Z -> Z] type function,
by applying it to each coordinate.*)
Fixpoint expand_fun1_list (f:Z -> Z) (t:list Z) : list Z :=
 match t with
 |nil => nil
 |a :: u => (f a) :: (expand_fun1_list f u)
 end.

Lemma expansion_fun1_soundness :
 forall (f:Z -> Z) (n:nat) (t:list Z),
  length t = n -> length (expand_fun1_list f t) = n.
Proof.
 induction n ; induction t ; simpl ; try discriminate ; try trivial.
 intro H0 ; injection H0 ; intro H1.
 rewrite (IHn t H1) ; reflexivity.
Qed.

Definition expand_fun1_vect (f:Z -> Z) {n:nat} (x:vect n) : vect n :=
 match x with
 |exist _ t H =>
  let u := expand_fun1_list f t in
  exist (fun l => length l = n) u (expansion_fun1_soundness f n t H)
 end.

(** ** Binary operations *)
(** Infer a [vect n -> vect n -> vect n] type function from a
[Z -> Z -> Z] type function, by applying it to each coordinate.*)
Fixpoint expand_fun2_list (f:Z -> Z -> Z) (t u : list Z) : list Z :=
 match t,u with
 |x :: t', y :: u' => (f x y) :: (expand_fun2_list f t' u')
 |_,nil => nil
 |nil,_ => nil
 end.

Lemma expansion_fun2_soundness :
 forall (f:Z -> Z -> Z) (n:nat) (t u:list Z), length t = n ->
  length u = n -> length (expand_fun2_list f t u) = n.
Proof.
 intro f.
 induction n ; induction t ; induction u ;
 simpl ; try discriminate ; try trivial.
 intros H0 H1 ; injection H0 ; injection H1 ; intros.
 simpl ; rewrite (IHn t u) ; try assumption.
 reflexivity.
Qed.

Definition expand_fun2_vect (f:Z -> Z -> Z) {n:nat} (x y:vect n) : vect n :=
 match x,y with
 |exist _ t H0, exist _ u H1 => 
  let v := expand_fun2_list f t u in
  exist (fun l => length l = n) v
  (expansion_fun2_soundness f n t u H0 H1)
 end.

(** ** Usual operators*)
Definition add {n:nat} : vect n -> vect n -> vect n := 
 expand_fun2_vect Zplus.

Definition sub {n:nat} : vect n -> vect n -> vect n := 
 expand_fun2_vect Zminus.

Definition opp {n:nat} : vect n -> vect n := 
 expand_fun1_vect Zopp.

Infix "|+" := add (at level 60).
Infix "|-" := sub (at level 60).
Notation "|- u" := (opp u) (at level 60).


(** * Predicates *)
(** ** Unary predicates *)
(** Infer a [vect n -> Prop] type predicate from a predicate [P] on [Z],
defined as the conjuction of [P] applied to each coordinate of [J].*)
Fixpoint expand_predicate1_list (J:nat -> Prop) (P:Z -> Prop)
 (t:list Z) : Prop :=
 match t with
 |nil => True
 |a :: u =>
  (J 0 -> P a) /\ expand_predicate1_list (fun n => J (S n)) P u
 end.

Fixpoint expand_predicate1 (J:nat -> Prop) (P:Z -> Prop)
 {n:nat} (x:vect n) : Prop :=
 expand_predicate1_list J P (get_list x).

(** ** Binary predicates *)
(** Infer a [vect n -> vect n -> Prop] type predicate from a
[Z -> Z -> Prop] type predicate [P], defined as the conjuction of P
applied to each coordinate of [J].*)
Fixpoint expand_predicate2_list (J:nat -> Prop) (P:Z -> Z -> Prop)
 (t u:list Z) : Prop :=
 match t,u with
 |nil,nil => True
 |a :: t', b :: u' =>
  (J 0 -> P a b) /\ (expand_predicate2_list (fun n => J (S n)) P t' u')
 |_,_ => False
 end.

Definition expand_predicate2 (J:nat -> Prop) (P:Z -> Z -> Prop)
 {n:nat} (x y:vect n) : Prop :=
 expand_predicate2_list J P (get_list x) (get_list y).

(** ** Usual predicates*)
Definition le_vect {n:nat} (J:nat -> Prop) (x y:vect n) : Prop :=
 expand_predicate2 J (Z.le) x y.

Definition lt_vect {n:nat} (J:nat -> Prop) (x y:vect n) : Prop :=
 le_vect J x y /\ exists p:nat, (J p /\ coord p x < coord p y)%Z.

Definition eq_list (J:nat -> Prop) (t u:list Z) : Prop :=
 expand_predicate2_list J (Z.eq) t u.

Definition eq_vect {n:nat} (J:nat -> Prop) (x y:vect n) : Prop :=
 expand_predicate2 J (Z.eq) x y.

Definition pos_vect {n:nat} (J:nat -> Prop) (x:vect n) : Prop :=
 expand_predicate1 J (fun p => Z.le 0 p) x.

Definition neg_vect {n:nat} (J:nat -> Prop) (x:vect n) : Prop :=
 expand_predicate1 J (fun p => Z.le p 0) x.


Infix "|<=" := (le_vect (fun n => True)) (at level 65).
Infix "|<" := (lt_vect (fun n => True)) (at level 65).
Notation "x |>= y" := (le_vect (fun n => True) y x) (at level 65).
Notation "x |> y" := (lt_vect (fun n => True) y x) (at level 65).

Notation "x |<=( J ) y" := (le_vect J x y) (at level 65).
Notation "x |<( J ) y" := (lt_vect J x y) (at level 65).
Notation "x |>=( J ) y" := (le_vect J y x) (at level 65).
Notation "x |>( J ) y" := (lt_vect J y x) (at level 65).

Notation "x |=( J ) y" := (eq_vect J x y) (at level 65).


(** * Merging *)
(** ** Vector merging *)
(** Merging two vectors by plugging one at the end of the other.*)
Lemma app_list_soundness :
 forall (n n':nat) (l l':list Z), length l = n -> length l' = n' ->
  length (l ++ l') = n + n'.
Proof.
 intros n n' l l' H H'.
 rewrite (List.app_length l l') ; rewrite H ; rewrite H' ; reflexivity.
Qed.

Definition merge_vect {n n':nat} (u:vect n) (u':vect n') : vect (n+n') :=
 match u,u' with
 |exist _ l H, exist _ l' H' => exist _ (l ++ l')
  (app_list_soundness n n' l l' H H')
 end.

Infix "|:" := merge_vect (at level 60).

(** ** Index set merging*)
Definition merge_index_set (d:nat) (J J':nat -> Prop) : nat -> Prop :=
 fun n => (n < d /\ J n) \/ (n >= d /\ J' (n - d)).

Notation "J :[ d ] J'" := (merge_index_set d J J') (at level 60).



(** * Projections *)
(** ** Upper projection *)
(** Projection on the first [p] components.*)
Fixpoint proj_up_list (p:nat) (t:list Z) : list Z :=
 match (p,t) with
 |(_,nil) => nil
 |(0,_) => nil
 |(S p, x :: u) => x :: (proj_up_list p u)
 end.

Lemma proj_up_list_soundness :
 forall (n p:nat) (t:list Z),
  length t = n -> p <= n -> length (proj_up_list p t) = p.
Proof.
 induction n ; destruct p ; destruct t ; simpl ; try reflexivity ;
 try discriminate.
 auto with arith.
 intros Ht Hp ; rewrite IHn.
 reflexivity.
 injection Ht ; trivial.
 auto with arith.
Qed.

Definition proj_up {n:nat} (p:nat) (Hp:p <= n) (x:vect n) : vect p :=
 match x with
 |exist _ l Hl => exist _ (proj_up_list p l)
  (proj_up_list_soundness n p l Hl Hp)
 end.

(** ** Lower projection *)
(** Projection on the last [p] components.*)
Fixpoint proj_down_list (p:nat) (t:list Z) : list Z :=
 match (p,t) with
 |(0,_) => t
 |(_, nil) => nil
 |(S p, x :: u) => proj_down_list p u
 end.

(* begin hide *)
Lemma proj_down_list_useful :
 forall (n p:nat) (t:list Z),
  length t = n -> p <= n -> length (proj_down_list p t) = n - p.
Proof.
 induction n ; destruct p ; destruct t ; simpl ; try reflexivity ;
 try discriminate ; trivial.
 intros Ht ; injection Ht ; intros ; apply IHn.
 assumption.
 auto with arith.
Qed.

Lemma nat_sub :
  forall (n p k:nat), k = p + n -> k - n = p.
Proof.
 induction n ; destruct p,k ; simpl ; try reflexivity ; try discriminate.
 +intro ; rewrite <- (plus_0_r p) ; assumption.
 +intro H ; injection H ; intro H' ; rewrite H' ; apply minus_diag.
 +intro H ; injection H ; intro H' ; apply (IHn (S p) k).
  rewrite plus_Snm_nSm ; assumption.
Qed.

Lemma nat_sub_le :
 forall (p n:nat), n - p <= n.
Proof.
 induction p ; destruct n ; simpl ; auto with arith.
Qed.
(* end hide *)

Lemma proj_down_list_soundness :
 forall (n p:nat) (t:list Z),
  length t = n -> p <= n -> length (proj_down_list (n - p) t) = p.
Proof.
 intros n p t Ht Hp.
 rewrite (proj_down_list_useful n (n - p) t).
 rewrite (minus_plus_simpl_l_reverse n (n - p) p).
 rewrite (le_plus_minus_r p n Hp).
 apply nat_sub ; reflexivity.
 assumption.
 apply nat_sub_le.
Qed.

Definition proj_down {n:nat} (p:nat) (Hp:p <= n) (x:vect n) : vect p :=
 match x with
 |exist _ l Hl => exist _ (proj_down_list (n - p) l)
  (proj_down_list_soundness n p l Hl Hp)
 end.



(** * Links between different notions of equality *)
(** To prove that two vectors are equals, it is sufficient to prove that the
lists that represent them are.*)
(* begin hide *)
Lemma one_length_proof :
 forall (n:nat) (l:list Z) (H0 H1:length l = n), H0 = H1.
Proof.
 intros n l H0 H1.
 apply eq_proofs_unicity.
 intros m p.
 destruct (eq_nat_dec m p) ; auto.
Qed.
(* end hide *)

Theorem eq_list_implies_eq_vect :
 forall {n:nat} (x y:vect n),
  get_list x = get_list y -> x = y.
Proof.
 destruct x,y.
 simpl.
 intro  H ; assert (x = x0).
 rewrite H ; reflexivity.
 subst ; f_equal.
 apply one_length_proof.  
Qed.

(** Partial equality is mere equality when considering all the coordinates.*)
(* begin hide *)
Lemma eq_list_equivalence :
 forall (t u:list Z),
  expand_predicate2_list (fun n => True) Z.eq t u -> t = u.
Proof.
 induction t.
 +induction u ; simpl ; [trivial | intro ; apply False_ind ; assumption].
 +intro u ; destruct u ; simpl.
  -intro ; apply False_ind ; assumption.
  -intro H ; elim H ; intros.
   rewrite (H0 I).
   rewrite (IHt u H1).
   reflexivity.
Qed.
(* end hide *)

Theorem eq_vect_soundness :
 forall {n:nat} (x y:vect n), x |=(fun n => True) y -> x = y.
Proof.
 intros n x y H.
 apply eq_list_implies_eq_vect.
 apply eq_list_equivalence.
 assumption.
Qed.