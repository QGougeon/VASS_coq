(** Vector projection properties. *)

(* Copyright (C) 2018 Quentin Gougeon

This programm is under GPL license version 3.

See https://www.gnu.org/licenses/gpl.html for details.*)

Require Export vector_def.
Require Import vector_group vector_order vector_merge List.

(** * Proof irrelevance*)
(** The upper projection doesn't depend on the proof that [p <= n].*)
Theorem proj_up_proof_irrelevance :
 forall {n:nat} (p:nat) (Hp Hp':p <= n) (x:vect n),
  proj_up p Hp x = proj_up p Hp' x.
Proof.
 intros ; destruct x.
 apply eq_list_implies_eq_vect ; simpl.
 reflexivity.
Qed.

(** The lower projection doesn't depend on the proof that [p <= n].*)
Theorem proj_down_proof_irrelevance :
 forall {n:nat} (p:nat) (Hp Hp':p <= n) (x:vect n),
  proj_down p Hp x = proj_down p Hp' x.
Proof.
 intros ; destruct x.
 apply eq_list_implies_eq_vect ; simpl.
 reflexivity.
Qed.

(* begin hide *)
Lemma proj_up_whole_list :
 forall t:list Z, proj_up_list (length t) t = t.
Proof.
 induction t ; simpl ; try (rewrite IHt) ; reflexivity.
Qed.
(* end hide *)

(** * Soundness properties*)
(** The upper projection on all the coordinates is the identity.*)
Theorem proj_up_whole :
 forall {n:nat} (x:vect n), proj_up n (Nat.le_refl n) x = x.
Proof.
 intro n ; destruct x.
 apply eq_list_implies_eq_vect ; simpl.
 rewrite <- e.
 apply proj_up_whole_list.
Qed.

(** The lower projection on all the coordinates is the identity.*)
Theorem proj_down_whole :
 forall {n:nat} (x:vect n), proj_down n (Nat.le_refl n) x = x.
Proof.
 intro n ; destruct x.
 apply eq_list_implies_eq_vect ; simpl.
 assert (n - n = 0).
  auto with arith.
 rewrite H.
 simpl ; reflexivity.
Qed.

(* begin hide *)
Lemma proj_down_whole_list :
 forall t:list Z, proj_down_list (length t) t = nil.
Proof.
 simpl.
 induction t ; simpl.
 reflexivity.
 assumption.
Qed.

Lemma proj_up_merge_list :
 forall (t u:list Z), proj_up_list (length t) (t ++ u) = t.
Proof.
 induction t ; destruct u ; repeat (rewrite app_nil_r) ;
 simpl ; try (rewrite proj_up_whole_list) ;
 try (rewrite (IHt (z :: u))) ; reflexivity.
Qed.

Lemma nat_useful_1 :
 forall n n':nat, n <= n + n'.
Proof.
 auto with arith.
Qed.
(* end hide *)

(** * Relations with merge*)
(** The upper projection of a merging is the upper vector.*)
Theorem proj_up_merge :
 forall {n n':nat} (x:vect n) (x':vect n'),
  proj_up n (nat_useful_1 n n') (x |: x') = x.
Proof.
 intro n ; destruct x,x'.
 apply eq_list_implies_eq_vect ; simpl.
 rewrite <- e.
 apply proj_up_merge_list.
Qed.

(* begin hide *)
Lemma proj_down_merge_list :
 forall (p:nat) (t u:list Z),
  length t = p -> proj_down_list p (t ++ u) = u.
Proof.
 induction p ; destruct t,u ; try (rewrite app_nil_r) ; simpl ;
 try reflexivity ; try discriminate.
 intro H ; injection H ; intro H' ; rewrite <- H' ; apply proj_down_whole_list.
 intro H ; injection H ; apply IHp.
Qed.

Lemma nat_useful_2 :
 forall n n':nat, n' <= n + n'.
Proof.
 auto with arith.
Qed.
(* end hide *)

(** The lower projection of a merging is the lower vector.*)
Theorem proj_down_merge :
 forall {n n':nat} (x:vect n) (x':vect n'),
  proj_down n' (nat_useful_2 n n') (x |: x') = x'.
Proof.
 intro n ; destruct x,x'.
 apply eq_list_implies_eq_vect ; simpl.
 rewrite <- e.
 apply proj_down_merge_list.
 apply eq_sym.
 apply nat_sub.
 reflexivity.
Qed.

(* begin hide *)
Lemma merge_proj_list :
 forall (p:nat) (t:list Z),
  p <= length t -> (proj_up_list p t) ++ (proj_down_list p t) = t.
Proof.
 induction p ; destruct t ; simpl ; try reflexivity.
 intro H ; rewrite (IHp t).
 reflexivity.
 auto with arith.
Qed.

Lemma rewrite_length :
 forall (n p:nat) (l:list Z),
  p <= n -> length l = p + (n - p) -> length l = n.
Proof.
 intros n p l Hp Hl ; rewrite Hl ; rewrite le_plus_minus_r.
 reflexivity.
 assumption.
Qed.
(* end hide *)

(** The merging of two complementary projections is the identity.*)
Definition change_type {n:nat} (p:nat) (Hp:p <= n) (x:vect (p + (n - p))) :
 vect n :=
 match x with
 |exist _ l Hl => exist _ l (rewrite_length n p l Hp Hl)
 end.

Theorem merge_proj :
 forall {n:nat} (p:nat) (x:vect n) (Hp:p <= n),
  change_type p Hp
   ((proj_up p Hp x) |: (proj_down (n - p) (nat_sub_le p n) x)) = x.
Proof.
 intros n p x Hp ; destruct x.
 apply eq_list_implies_eq_vect ; simpl.
 assert (n - (n - p) = p).
 {
  rewrite (minus_plus_simpl_l_reverse n _ p).
  rewrite le_plus_minus_r.
  apply nat_sub ; reflexivity.
  assumption.
 }
 rewrite H.
 apply merge_proj_list.
 rewrite e ; assumption.
Qed.

(* begin hide *)
Lemma add_proj_up_list :
 forall (p:nat) (t u:list Z),
  proj_up_list p (addl t u) = addl (proj_up_list p t) (proj_up_list p u).
Proof.
 induction p ; destruct t,u ; unfold addl ; simpl ; try reflexivity.
 repeat (rewrite addl_useful).
 rewrite (IHp t u) ; reflexivity.
Qed.
(* end hide *)

(** * Group-related properties*)
(** Upper projection is a morphism.*)
Theorem add_proj_up :
 forall {n:nat} (p:nat) (Hp:p <= n) (x y:vect n),
  proj_up p Hp (x |+ y) = (proj_up p Hp x) |+ (proj_up p Hp y).
Proof.
 intros n p Hp ; destruct x,y.
 apply eq_list_implies_eq_vect ; simpl.
 rewrite addl_useful.
 rewrite (addl_useful (proj_up_list _ _) _).
 apply add_proj_up_list.
Qed.

(* begin hide *)
Lemma opp_proj_up_list :
 forall (p:nat) (t:list Z),
  proj_up_list p (oppl t) = oppl (proj_up_list p t).
Proof.
 induction p ; destruct t ; unfold oppl ; simpl ; try reflexivity.
 rewrite oppl_useful.
 rewrite (IHp t).
 reflexivity.
Qed.
(* end hide *)

(** Other morphism properties.*)
Theorem opp_proj_up :
 forall {n:nat} (p:nat) (Hp:p <= n) (x:vect n),
  proj_up p Hp (|- x) = |- (proj_up p Hp x).
Proof.
 intros n p ; destruct x.
 apply eq_list_implies_eq_vect ; simpl.
 apply opp_proj_up_list.
Qed.

Theorem sub_proj_up :
 forall {n:nat} (p:nat) (Hp:p <= n) (x y:vect n),
  proj_up p Hp (x |- y) = (proj_up p Hp x) |- (proj_up p Hp y).
Proof.
 intros.
 rewrite <- sub_soundness.
 rewrite <- sub_soundness.
 rewrite add_proj_up.
 rewrite opp_proj_up.
 reflexivity.
Qed.

Theorem proj_up_zero :
 forall {n:nat} (p:nat) (Hp:p <= n),
  proj_up p Hp (null_vect (n := n)) = null_vect (n := p).
Proof.
 intros n p Hp.
 rewrite <- (sub_inverse ((null_vect (n := n)))).
 rewrite sub_proj_up.
 apply sub_inverse.
Qed.

(* begin hide *)
Lemma add_proj_down_list :
 forall (p:nat) (t u:list Z),
  proj_down_list p (addl t u) = addl (proj_down_list p t) (proj_down_list p u).
Proof.
 induction p ; destruct t,u ; simpl ; try reflexivity.
 case (proj_down_list p u) ; reflexivity.
 case (proj_down_list p t) ; reflexivity.
 apply IHp.
Qed.
(* end hide *)

(** Lower projection is a morphism.*)
Theorem add_proj_down :
 forall {n:nat} (p:nat) (Hp:p <= n) (x y:vect n),
  proj_down p Hp (x |+ y) = (proj_down p Hp x) |+ (proj_down p Hp y).
Proof.
 intros n p Hp ; destruct x,y.
 apply eq_list_implies_eq_vect ; simpl.
 rewrite addl_useful.
 rewrite (addl_useful (proj_down_list _ _) _).
 apply add_proj_down_list.
Qed.

(* begin hide *)
Lemma opp_proj_down_list :
 forall (p:nat) (t:list Z),
  proj_down_list p (oppl t) = oppl (proj_down_list p t).
Proof.
 induction p ; destruct t ; unfold oppl ; simpl ; try reflexivity.
 rewrite oppl_useful.
 rewrite (IHp t).
 reflexivity.
Qed.
(* end hide *)

(** Other morphism properties.*)
Theorem opp_proj_down :
 forall {n:nat} (p:nat) (Hp:p <= n) (x:vect n),
  proj_down p Hp (|- x) = |- (proj_down p Hp x).
Proof.
 intros n p ; destruct x.
 apply eq_list_implies_eq_vect ; simpl.
 apply opp_proj_down_list.
Qed.

Theorem sub_proj_down :
 forall {n:nat} (p:nat) (Hp:p <= n) (x y:vect n),
  proj_down p Hp (x |- y) = (proj_down p Hp x) |- (proj_down p Hp y).
Proof.
 intros.
 rewrite <- sub_soundness.
 rewrite <- sub_soundness.
 rewrite add_proj_down.
 rewrite opp_proj_down.
 reflexivity.
Qed.

Theorem proj_down_zero :
 forall {n:nat} (p:nat) (Hp:p <= n),
  proj_down p Hp (null_vect (n := n)) = null_vect (n := p).
Proof.
 intros n p Hp.
 rewrite <- (sub_inverse ((null_vect (n := n)))).
 rewrite sub_proj_down.
 apply sub_inverse.
Qed.

(* begin hide *)
Lemma coord_proj_up_list :
 forall (p k:nat) (t:list Z), p <= length t -> k < p ->
 coord_list k (proj_up_list p t) = coord_list k t.
Proof.
 unfold coord_list ; induction p ; destruct k,t ; simpl ; try reflexivity.
 +assert (~(0 < 0)).
   auto with arith.
  intros ; apply False_ind ; elim H ; assumption.
 +intros ; assert (~(S k < 0)).
   auto with arith.
  intros ; apply False_ind ; elim H1 ; assumption.
 +intros ; apply (IHp k t) ; auto with arith.
Qed.
(* end hide *)

(** * Properties involving the coordinates*)
(** Compatibility of upper projection with coord.*)
Theorem coord_proj_up :
 forall {n:nat} (p k:nat) (Hp:p <= n) (x:vect n),
  k < p -> coord k (proj_up p Hp x) = coord k x.
Proof.
 intros n p k Hp x Hk ; destruct x ; unfold coord ; simpl. 
 apply coord_proj_up_list ; try (rewrite e) ; assumption.
Qed.

(* begin hide *)
Lemma coord_proj_down_list :
 forall (p k:nat) (t:list Z), p < length t -> k < length t - p ->
 coord_list k (proj_down_list p t) = coord_list (p + k) t.
Proof.
 unfold coord_list ; induction p ; destruct k,t ; simpl ; try reflexivity ;
 intros ; apply IHp ; auto with arith.
Qed.

Lemma nat_useful_3 :
 forall n k p:nat, k < p -> p <= n -> n - p < n.
Proof.
 assert (forall q:nat, ~(q < 0)).
  auto with arith.
 induction n ; destruct k,p.
 +intros ; apply False_ind ; elim (H 0) ; assumption.
 +intros ; simpl ; apply False_ind ; elim (H 0).
  apply (lt_le_trans 0 (S p) 0) ; assumption.
 +intros ; simpl ; apply False_ind ; elim (H (S k)) ; assumption.
 +intros ; simpl ; apply False_ind ; elim (H (S k)).
  apply (lt_le_trans _ (S p) _) ; assumption.
 +intros ; apply False_ind ; elim (H 0) ; assumption.
 +destruct p ; intros ; auto with arith.
 +intros ; apply False_ind ; elim (H (S k)) ; assumption.
 +intros ; simpl ; apply (Nat.lt_trans _ n _).
  apply (IHn k p) ; auto with arith.
  apply le_lt_n_Sm ; auto with arith.
Qed.
(* end hide *)

(** Compatibility of lower projection with coord.*)
Theorem coord_proj_down :
 forall {n:nat} (p k:nat) (Hp:p <= n) (x:vect n),
  k < p -> coord k (proj_down p Hp x) = coord (n - p + k) x.
Proof.
 intros n p k Hp x Hk ; destruct x ; unfold coord ; simpl.
 apply coord_proj_down_list.
 rewrite e ; apply (nat_useful_3 n k p) ; auto with arith.
 rewrite e ; simpl ; auto with arith.
 rewrite <- (plus_minus n (n - p) p).
 assumption.
 rewrite Nat.add_comm.
 apply le_plus_minus.
 assumption.
Qed.

(** * Order-related properties*)
(** Compatibility of upper projection with [<=].*)
Theorem le_proj_up :
 forall {n:nat} (p:nat) (Hp:p <= n) (J:nat -> Prop) (x y:vect n),
  x |<=(J) y -> (proj_up p Hp x) |<=(fun k => J k /\ k < p) (proj_up p Hp y).
Proof.
 intros n p Hp J x y Hle.
 apply le_coord_to_vect.
 intros k Hk ; elim Hk ; intros H0 H1.
 repeat (rewrite coord_proj_up) ; try assumption.
 apply (le_vect_to_coord (J := J)) ; assumption.
Qed.
 
(** Compatibility of lower projection with [<=].*)
Theorem le_proj_down :
 forall {n:nat} (p:nat) (Hp:p <= n) (J:nat -> Prop) (x y:vect n),
  x |<=(J) y ->
   (proj_down p Hp x) |<=(fun k => J (n - p + k) /\ k < p) (proj_down p Hp y).
Proof.
 intros n p Hp J x y Hle.
 apply le_coord_to_vect.
 intros k Hk ; elim Hk ; intros H0 H1.
 repeat (rewrite coord_proj_down) ; try assumption.
 apply (le_vect_to_coord (J := J)) ; assumption.
Qed.