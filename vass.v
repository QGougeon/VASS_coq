(** VASS module. *)

(* Copyright (C) 2018 Quentin Gougeon

This programm is under GPL license version 3.

See https://www.gnu.org/licenses/gpl.html for details.*)

Require Export TS vector_def.
Require Import vector_group.

(** * Main definitions*)
Definition Q (N:Z) := {n:Z | (1 <= n <= N)%Z}.
Definition cnfs {d:nat} (N:Z) : Type := (Q N) * (vect d).

Definition state {d:nat} {N:Z} : cnfs (d:=d) N-> Q N := fst.
Definition value {d:nat} {N:Z} : cnfs N -> vect d := snd.

Definition is_vass_tr_fun {d:nat} {label : Type} (N:Z)
 (f:cnfs N -> label -> cnfs N -> Prop) : Prop :=
 exists (marking:Q N -> vect d -> Q N -> option label),
  forall (c c':cnfs N) (a:label),
  f c a c' <-> marking (state c) (value c' |- value c) (state c') = Some a.

Definition is_vass {d:nat} {label AP:Type} (N:Z)
  (s:TS (cnfs N) label AP) : Prop :=
 is_vass_tr_fun (d := d) (label := label) N tr_fun.

Definition mk_vass {d:nat} {label AP:Type} (N:Z) (is_cnfs:cnfs N -> Prop)
 (marking:Q N -> vect d -> Q N -> option label) (val:cnfs N -> AP -> Prop) :
  TS (cnfs N) label AP :=
 mkTS (cnfs N) label AP is_cnfs
  (fun c a c' =>
   marking (state c) (value c' |- value c) (state c') = Some a) val.

Lemma mk_vass_soundness :
 forall {d:nat} {label AP:Type} (N:Z) (is_cnfs:cnfs N -> Prop)
 (marking:Q N -> vect d -> Q N -> option label) (val:cnfs N-> AP -> Prop),
 is_vass N (mk_vass N is_cnfs marking val).
Proof.
 intros d label AP N is_cnfs marking val.
 try (exists marking) ; simpl ; split ; trivial.
Qed.